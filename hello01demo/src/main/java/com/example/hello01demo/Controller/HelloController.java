package com.example.hello01demo.Controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {
    @GetMapping("/hello")
    public String hello(){
        return "hello Jenkins! <br>  软件工程1901班 <br> 27<br> 郑彤";
    }
}
